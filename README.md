# aws_cli_mfa



## Getting started
```
python auth.py -h
usage: auth.py [-h] (--op | --totp TOTP) --mfa MFA --profile PROFILE [--opitem OPITEM]

options:
  -h, --help         show this help message and exit
  --op
  --totp TOTP
  --mfa MFA
  --profile PROFILE
  --opitem OPITEM
```

### What
Easily generate a session token using MFA that can be used for further AWS CLI commands.

Supports getting OTP value from 1password or manually provided TOTP code

### Why
Using the AWS CLI with credentials on an account that requires MFA requires users to get a valid session token using AWS Security Token Service. This requires running the following command:
```
aws sts get-session-token --serial-number <ARN of MFA device on your account> --token-code <TOTP code>
```
then taking the access_key_id, secret_access_key, and session_token values from the output and adding them to a new or existing profile. This is a complex process which is error prone and time consuming. 

### How
Clone the repository

```
pip install -r requirements.txt
```

MFA auth using value from 1password

```
eval "$(python auth.py --op --opitem "AWS" --mfa  <MFA device ARN>  --profile <AWS CLI profile name>)"
```

MFA auth using manually provided TOTP value

```
eval "$(python auth.py --totp 402056 --mfa <MFA device ARN> --profile <AWS CLI profile name>)"
```

Check the environment variables produced for AWS CLI authentication

```
printenv | grep AWS
```

Add a wrapper function to your shell (e.g. ~/.zshrc). This example only works for 1password integration to obtain mfa token code.

```
function auth () {
  op vault ls >> /dev/null
  case $1 in
    production|prod)
      auth -d
      export OP_ITEM="<1Password Item Name>"
      export MFA_DEVICE=$(op item get "$OP_ITEM" --fields label="mfa_arn")
      export AWS_ACCESS_KEY_ID=$(op item get "$OP_ITEM" --fields label="access key id")
      export AWS_SECRET_ACCESS_KEY=$(op item get "$OP_ITEM" --fields label="secret access key")
      export AWS_DEFAULT_REGION=$(op item get "$OP_ITEM" --fields label="default region")
      export AWS_PAGER=''
      export AWS_VAULT=production
      eval "$(python ~/Code/kryterion/aws_cli_mfa/auth.py --op --opitem $OP_ITEM --mfa $MFA_DEVICE)"
      ;;

    development|dev)
      auth -d
      export OP_ITEM="<1Password Item Name>"
      export MFA_DEVICE=$(op item get "$OP_ITEM" --fields label="mfa_arn")
      export AWS_ACCESS_KEY_ID=$(op item get "$OP_ITEM" --fields label="access key id")
      export AWS_SECRET_ACCESS_KEY=$(op item get "$OP_ITEM" --fields label="secret access key")
      export AWS_DEFAULT_REGION=$(op item get "$OP_ITEM" --fields label="default region")
      export AWS_PAGER=''
      export AWS_VAULT=development
      eval "$(python ~/Code/kryterion/aws_cli_mfa/auth.py --op --opitem $OP_ITEM --mfa $MFA_DEVICE)"
      ;;

    -d)
      unset AWS_PROFILE
      unset AWS_ACCESS_KEY_ID
      unset AWS_SECRET_ACCESS_KEY
      unset AWS_SESSION_TOKEN
      unset AWS_DEFAULT_REGION
      unset AWS_PAGER
      unset OP_ITEM
      return 0
      ;;

    -s)
      set | grep AWS
      ;;

    -h)
      auth_usage
      ;;

    *)
      echo -e "Unsupported argument: $1 \n"
      auth_usage
      ;;
  esac

}

auth_usage() {
  echo -e "Usage:\n"
  echo -e "auth [prod|dev]  - set AWS environment variables using 1password to obtain credentials"
  echo -e "auth [-d]        - unset AWS environment variables and invalidate AWS session"
  echo -e "auth [-s]        - show current AWS environment variables"
  echo -n "auth [-h]        - print this help message"
}
```

Call the function
```
auth <env>
auth prod
auth development
```

Clear aws session
```
auth -d
```