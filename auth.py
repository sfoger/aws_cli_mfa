#!/usr/bin/env python
import os
import boto3
import subprocess
import argparse

def main():
    parser = argparse.ArgumentParser()
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('--op', action='store_true')
    group.add_argument('--totp')
    parser.add_argument('--mfa', required=True)
    parser.add_argument('--profile', required=False)
    parser.add_argument('--opitem')
    args = parser.parse_args()
    session = boto3.Session(profile_name=args.profile) if args.profile else boto3.Session()
    client = session.client("sts")
    if args.op:
        if args.opitem:
            response = client.get_session_token(
                DurationSeconds=3600,
                SerialNumber=args.mfa,
                TokenCode=subprocess.run(
                        ['op', 'item', 'get', args.opitem, '--otp'], 
                        capture_output=True
                    ).stdout.strip().decode("utf-8")
                )
    elif args.totp:
        response = client.get_session_token(
            DurationSeconds=3600,
            SerialNumber=args.mfa,
            TokenCode=args.totp
            )

    access_key_id = response.get('Credentials').get('AccessKeyId')
    secret_access_key = response.get('Credentials').get('SecretAccessKey')
    session_token = response.get('Credentials').get('SessionToken')
    expiration = response.get('Credentials').get('Expiration')
    
    res = []
    res.append(f'export AWS_ACCESS_KEY_ID={access_key_id}')
    res.append(f'export AWS_SECRET_ACCESS_KEY={secret_access_key}')
    res.append(f'export AWS_SESSION_TOKEN={session_token}')
    res.append(f'declare -x AWS_SESSION_EXPIRATION=\'{expiration}\'')

    return print('\n'.join(res))

if __name__ == "__main__":
    main()
